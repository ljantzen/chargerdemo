---?image=assets/Jigsaw-Duke-2-1.png&size=auto 30%&position=right 25% bottom 95%;
@title[Java 9 Module System]

## Java 9 Module System

 <div style="float:right">
 	Leif Jantzen<br/> 
 	@jantzen
 </div> 

![Logo](assets/webstep-logo-small.png)


---?image=assets/buzz.png&size=auto 30%&position=right 80%;

@title[Introduksjon]

## java9 - To infinity and beyond

- Java 9 sluppet oktober 2017. 3 år etter Java 8.
- Java 10 sluppet mars 2018.
- Java 11 kommer neste uke
- 2 releaser per år, mars og september
- Java 8 og 11 LTS versjoner, 3 års syklus

---

@title[Hva er modularitet?]

## Hva er en modul? 

- "En klump" programvare
- Har et navn
- Har et veldefinert grensesnitt
- Innkapsler intern implementasjon
- Har eksplisitte avhengigheter til én eller flere moduler

---

@title[Pre java 9]

## Pre java 9 

Er en jar-fil en modul? 

- Har et navn
- Innkapsler ikke intern implementasjon
- Har kanskje et veldefinert grensesnitt
- Har ikke eksplisitte avhengigheter 


---
@title[JDK Monolitt]

## JDK/JVM er en monolitt

- Alt er inkludert
- Ingenting er optional
- Hele runtime-biblioteket i rt.jar (61 Mb i j8)
- Alt public er public for alle 
- Protected/private kan nås med reflection
- Alt er avhengig av alt 

+++?image=assets/bbom.jpeg&size=auto 90%

@title[Big ball of mud]

---

@title[JDK7 Dependency graph]

## JDK7 pakkeavhengigheter

![JDK7 Dependency graph](assets/jdk7-dependencies.png)

+++?image=assets/monolith.jpg&size=cover&position=right 80%; 
@title[Breaking the monolith]

 ### Hvordan deler man dette opp i moduler?

---
@title[Andre modulsystemer]
## Men det finnes jo allerede modulsystemer 

- OSGI
- JBoss Modules

- Kan ikke brukes internt i JDK
- Løser andre problemer
- Inkludert i ekspertgruppen, men ikke utpreget harmonisk samarbeid.  

Note:

On the other hand, JMPS is much simpler than OSGi and as far as I can tell addresses a different use-case. With JPMS modules, 1) searching for classes is likely to be faster 2) self contained applications will become feasable at last.

While it is difficult to see how JMPS will facilitate the creation of "big" applications composed of many components, JMPS will allow applications to be assembled as stand-alone programs rather than WAR/EAR files to be deployed in Application Servers.

---
@title[Hvorfor først i java 9]
## Hvorfor først i java9? 

- Manglende modularitet internt i JDK/JVM et problem for Oracle
- Komplekst, var allerede på roadmap for jdk 7, men utsatt 
- Oppsplitting av eksisterende kode i moduler vanskelig å få til 
- Tok 3 år

---
@title[Moduler 101]
## Java Modules 101 

- Nye konsepter
- Nye verktøy

---
@title[Modul konsepter]
## Konseptuelt

- Klasser enkapsulerer metoder og felt 
- Pakker enkapsulerer klasser	

- Moduler enkapsulerer pakker
- Moduler har avhengigheter 
- Moduler eksponerer pakker 
- Moduler skjuler interne implementasjonspakker 

+++?image=assets/modularhouse.jpg&size=30%&position=right 80%; 
## Moduler bor i module-path

- Classpath finnes fortsatt 
- Module-path spesifiseres med ny kommandolinjeparameter -m
- Inneholder jar-filer som har modul metadata


+++ 
@title[Readability]
## Readability

En modul som er avhengig av en annen modul 'leser' (reads) den andre modulen , som er readable for den første.

Da har den tilgang til det som modulen eksporterer. 

Det som ikke blir eksportert ut av en modul, er ikke tilgjengelig, selv med reflection. 

---
@title[Modul graf]
## Modulene danner en graf

Under oppstart leses modul-descriptorene, og det bygges opp et tre av modul-avhengigheter. 

Det sjekkes så at alle nødvendige moduler finnes på module-path.

No more ClassDefNotFoundException. 

---
@title[4 Modul-typer]

## 4 Modul-typer 

- System Modules - Java SE og JDK moduler
- Application Modules – jar fil på module-path med module-deskriptor 
- Automatic Modules – Jar filer som ligger i module-path 
- Unnamed Module – Alle jar-filer som ligger på classpath er i en standard modul uten navn

+++
@title[Liste alle moduler]

## Hvilke moduler finnes i jdk? 

```
java --list-modules | wc -l 

98
``` 
	

---
@title[Modul-descriptor]
## Hva er en modul-descriptor?

- Heter module-info.java
- Er ikke java-kode, men et eget DSL
- Må ligge på toppen av modul-source treet

---
@title[module-info.java 1]
## module-info.java

```lang=java
module minmodul {
}```

Avhengighet til modulen java.base er implisitt, og kan utelates

+++
@title[module-info.java 2]
## module-info.java

```lang=java
module minmodul {
  requires java.sql;
  requires java.logging;  
}```

En modul spesifiserer hvilke moduler den er avhengig av 

+++
@title[transitive avhengigheter]
## Transitive avhengigheter

```lang=java
module minmodul {
  requires transitive java.sql;
  requires java.logging;  
}```

Alle moduler som requires minmodul vil også ha tilgang på pakkene i java.sql modulen

+++
@title[Open modules]
## Open moduler

Hva med moduler med pakker som må aksesseres via reflection?

```lang=java
module minmodul {
  requires java.logging;  

  // til alle moduler
  opens no.webstep.minpakke; 
  
  // eller en spesifikk modul
  opens no.webstep.enannen.pakke to java.xml; 
}```

Her gir vi modulen java.xml tilgang til å gjøre reflection på klassene i en spesifikk pakke 

---
@title[Kompilering]
## Kompilering av en modul 

javac –module-source-path src -d target -m minmodul 

@color[grey](Kompilerer modulen minmodul, leter etter sourcen i src katalogen)

---?image=assets/demotime.jpg&size=50%&position=right 80%; 


---
@title[Moduler 201]
## Java Modules 201 

---
@title[Services og providers]
## Services og providers 

- Løsner på tight coupling mellom moduler
- Et interface kan eksporteres av en modul og implementeres av flere andre moduler
- En ServiceFactory brukes til å instansiere implementasjoner
- Implementasjoner resolves under oppstart, avhengig av hva som ligger i module-path

+++
@title[Service]
## Service-definisjon
```lang=java
  package my.service.api;
  public interface MyService { boolean doWork(); }
```

```lang=java
  module MyService {
    requires MyCoreServices;
    exports my.service.api;
  }
```

+++
@title[Provider]
## Provider-definisjon

```lang=java
  module MyServiceProvider {
    requires MyService;
    provides my.service.api.MyService with 
      my.service.api.MyServiceImplementation;
  }
```

```lang=java
  package my.service.impl;
  public class MyServiceImplementation { boolean doWork(){...} };
```

+++
@title[Consumer]
## Consumer-definisjon

```lang=java
  module MyServiceConsumer {
    requires MyService;
    uses my.service.api.MyService ;
  }
```

+++
@title[Bruk av providers]
## Bruke providers

```lang=java
public class Consumer {

    public static void main(String[] args) {
        ServiceLoader<ServiceInterface> loader = ServiceLoader.load(ServiceInterface.class);
        for (final ServiceInterface service : loader) {
            service.doWork();
        }
    }
}
``` 

---
@title[Nye verktøy]
## Nye verktøy

- JDeps 
    - finner avhengigheter
    - Introdusert i jdk8, men utvidet for moduler i jdk9

- Jlink
    - fjerner pakker som ikke brukes av applikasjonen
    - genererer ny JAVA_HOME med bare det som trengs 
    - genialt for docker-images og AWS Lambda deployment
    
---
@title[JDeps]
## JDeps

1    $JAVA9_HOME/bin/jdeps mymodule.jar

@color[grey](lister ut alle dependencies til en modul) 	
	
2    $JAVA9_HOME/bin/jdeps --class-path target/lib/*.jar mymodule.jar 

@color[grey](ta med pakkker i den navnløse modulen, ie classpath) 
     
3    $JAVA9_HOME/bin/jdeps -p no.webstep.iot.utils mymodule.jar

@color[grey](lister moduler som har pakken som avhengighet) 

---
@title[JLink]
## Jlink

Gitt koden i 

```
class Test  {
    public static void main(String[]args) {
        System.out.prinltn("Hello World");
    } 
}```

Her trenger JVM'en klassene Test, String, System og Objekt for å kunne kjøre programmet.
De 4300 andre klassene i JDK kan trygt fjernes. 

+++

Ved å fjerne disse, vil programmet 

@ul 

- Starte raskere
- Bruke mindre minne
- Kunne brukes i andre miljøer enn der jvm typisk blir brukt

@ulend

--- 
@title[JLink eksempler]

## Et eksempel

    jlink --module-path  \ 
          --add-modules com.example.testapp \ 
          --limit-modules com.example.testapp \
          --output outputdir   


    jlink --module-path exampledir:$MODS \ 
          --add-modules com.example.testapp \ 
          --limit-modules com.example.testapp \
          --output outputdir   
          
$MODS er katalogen hvor jdk systemmodulene ligger, typisk i $JAVA_HOME/jmods         

+++ 

JLink kan ikke brukes hvis applikasjonen benytter automatiske moduler 

+++ 

Java9 introduserte et nytt filformat jmod, som er en zip-fil med 

- class-filer 
- modul metadata 
- brukes av jlink

---
@title[Moditect]

## Moditect 

- Generating module-info.java descriptors for given artifacts (Maven dependencies or local JAR files)
- Adding module descriptors to your project's JAR as well as existing JAR files (dependencies)
- Creating module runtime images

https://github.com/moditect/moditect


---
@title[Migrasjon]
## Migrere til java 9+ 

- "With care and deliberation"

+++
- --permit-illegal-access slår av hele modul-systemet

+++
- noen klasser i jdk har blitt utilgjengelige, f.eks sun.misc.Unsafe.
  Disse finnes nå i modulen jdk.unsupported
+++
- samme pakke kan ikke forekomme i mer enn en modul 
  Ikke mulig å 'legge til' klasser i jdk-pakker
  
+++

- Oppgrader til verktøy og biblioteker som støtter JDK 9
    - maven versjon 3.5.0
    - maven compiler plugin 3.7.0 
    - https://blog.codefx.org/tools/maven-on-java-9/
    - gradle versjon 4.2.1     
    - https://guides.gradle.org/building-java-9-modules/
- Oppgradering av Java EE funksjonalitet
    - EE ut av jdk     

+++

Bruk kommandolinjeparametrene 

- --add-module 
- --add-exports
- --add-opens
- --add-modules 
- --add-reads
- --patch-module

til å gi modulene tilgangene til de klassene de trenger 

https://blog.codefx.org/java/five-command-line-options-to-hack-the-java-9-module-system/

+++

- Sjekk loggene for "illegal reflective access" advarsler, send inn bug-meldinger
- Testing, testing, testing 
- Kjør jdeps for å sjekke om du eller 3.part benytter interne apier
- Sjekk kode som parser java versjonsstrengen 
- Følg med på release notes

---
@title[Adopsjon]

## Er det noen som bruker dette, da?

- Java 9+ har ikke tatt av, for å si det pent
- Ca 10% har 9+ i produksjon
- Av disse har 10-20% benyttet modulsystemet
- Java er det eneste store systemet som har tatt jspm i bruk 

+++

_It is too early to say wether modules will achieve widespread adoption outside the JDK itself. In the meantime, it seems best to see for yourself wether a modular approach fits your codebase._


+++?image=assets/anewhope.png&size=auto 90%
